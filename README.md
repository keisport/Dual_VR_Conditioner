# Dual VR Conditioner

LM1815-based SMD VR conditioner.

BoM is on octopart:
https://octopart.com/bom-tool/a1F8JgLb

PCBs are on OSHpark:
https://oshpark.com/shared_projects/qtJiOrHv
